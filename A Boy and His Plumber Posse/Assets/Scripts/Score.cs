﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Score : MonoBehaviour
{
    public Text player1;
    public Text player2;
    public Text player3;
    public Text player4;

    public int player1Score;
    public int player2Score;
    public int player3Score;
    public int player4Score;


    // Use this for initialization
    void Start ()
    {
        player1Score = 0;
        player2Score = 0;
        player3Score = 0;
        player4Score = 0;
    }
	
	// Update is called once per frame
	void Update ()
    {
        player1.text = "$" + player1Score.ToString();
        player2.text = "$" + player2Score.ToString();
        player3.text = "$" + player3Score.ToString();
        player4.text = "$" + player4Score.ToString();
    }

    public void updateScore(int score, int playerNumber)
    {
        if(playerNumber == 1)
        {
            player1Score += score; 

            if (score < 0)
            {
                player1.color = Color.red;
            } else
            {
                player1.color = Color.green;
            }
        }

        else if (playerNumber == 2)
        {
            player2Score += score;

            if (score < 0)
            {
                player2.color = Color.red;
            }
            else
            {
                player2.color = Color.green;
            }
        }

        else if (playerNumber == 3)
        {
            player3Score += score;

            if (score < 0)
            {
                player3.color = Color.red;
            }
            else
            {
                player3.color = Color.green;
            }
        }

        else if (playerNumber == 4)
        {
            player4Score += score;

            if (score < 0)
            {
                player4.color = Color.red;
            }
            else
            {
                player4.color = Color.green;
            }
        }
    }
}
