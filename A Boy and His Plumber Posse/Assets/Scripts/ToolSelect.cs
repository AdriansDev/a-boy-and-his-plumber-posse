﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using XboxCtrlrInput;

public class ToolSelect : MonoBehaviour {
    public int toolnum = 0;

    //public PlayerPickup playerpickup;
    public BaseWater basewater;
    public float EvaporatorMultiplyer = 1.0f;
    public bool toolbox = false;
    public int FixedHits = 0;
    public bool isInRange;
    public HandSlot slot;

    private GameObject currentPipe;

    // Use this for initialization
    void Start ()
    {
        slot.plunger.SetActive(false);
        slot.Vacuum.SetActive(false);
        slot.wrench.SetActive(false);
        slot.Ducktape.SetActive(false);
    }
	
	// Update is called once per frame
	void Update ()
    {

        switch (toolnum)
        {
            default:
                break;
            case 0:
                Plunger();
                break;
            case 1:
                Evaporator();
                break;
            case 2:
                DuctTape();
                break;
            case 3:
                Wrench();
                break;
        }

        if(toolbox)
        {
            if(XCI.GetButtonDown(XboxButton.RightBumper))
            {
                toolnum++;
                if(toolnum > 3)
                {
                    toolnum = 0;
                }
            }
        }
        else
        {

        }

    }


    void Plunger()
    {
        slot.plunger.SetActive(true);
        slot.Vacuum.SetActive(false);
        slot.wrench.SetActive(false);
        slot.Ducktape.SetActive(false);
        GetComponentInChildren<PlayerPickup>().SetPlungerOn(true);
    }

    void Evaporator()
    {
        slot.plunger.SetActive(false);
        slot.Vacuum.SetActive(true);
        slot.wrench.SetActive(false);
        slot.Ducktape.SetActive(false);
        GetComponentInChildren<PlayerPickup>().SetPlungerOn(false);
        if (XCI.GetButton(XboxButton.LeftBumper))
        {
            slot.Flame.SetActive(true);
            //--basewater.GetComponent<BaseWater>().WaterLevel;
        }
        else
        {
            slot.Flame.SetActive(false);
        }
       
    }

    void DuctTape()
    {
        slot.plunger.SetActive(false);
        slot.Vacuum.SetActive(false);
        slot.wrench.SetActive(false);
        slot.Ducktape.SetActive(true);
        GetComponentInChildren<PlayerPickup>().SetPlungerOn(false);
        if (XCI.GetButtonDown(XboxButton.LeftBumper))
        {

            if (isInRange)
            {
                FixedHits++;
                if (FixedHits >= 3)
                {
                    currentPipe.GetComponent<DripSwitch>().isFixed = true;
                    Debug.Log("is fixed");
                }
            }
            else
            {

            }
        }
    }

    void Wrench()
    {
        slot.plunger.SetActive(false);
        slot.Vacuum.SetActive(false);
        slot.wrench.SetActive(true);
        slot.Ducktape.SetActive(false);
        GetComponentInChildren<PlayerPickup>().SetPlungerOn(false);
        if (XCI.GetButtonDown(XboxButton.LeftBumper))
        {
            if (isInRange)
            {
                FixedHits++;
                if (FixedHits >= 5)
                {
                    currentPipe.GetComponent<DripSwitch>().isFixed = true;
                    Debug.Log("is fixed");
                }
            }
            else
            {

            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "WaterParticle")
        {
            isInRange = true;

            currentPipe = other.gameObject;
        }
    }
    private void OnTriggerExit(Collider other)
    {
        isInRange = false;
        FixedHits = 0;
        currentPipe = null;
    }

}
