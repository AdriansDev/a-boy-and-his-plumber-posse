﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ManagerOfGame : MonoBehaviour {

    public float Timer;
    public float waterTimer;
    public int GameLength;

    public GameObject[] PS;
    public GameObject water;

    WallExplosion explosionSystem;
    BaseWater BW;
    public DripSwitch[] DS;

    public GameObject displayEndgame;
    public Text endgame;
    public Score myScoreScript;

    int length;

	// Use this for initialization
	void Start ()
    {
        displayEndgame.SetActive(false);
        Timer = 0.0f;
        waterTimer = 0.0f;
        explosionSystem = new WallExplosion();

        PS = GameObject.FindGameObjectsWithTag("WaterParticle");
        BW = water.GetComponent<BaseWater>();

        length = PS.Length;
	}
	
	// Update is called once per frame
	void Update ()
    {
        Timer += Time.deltaTime;
        waterTimer += Time.deltaTime;

        if(waterTimer >= 2.0f)
        {
            float time = 0.0f;
            DripSwitch drsw = PS[Random.Range(0, PS.Length)].GetComponent<DripSwitch>();
     
            if (drsw == null && time <= 5)
            {
                DripSwitch drsw2 = PS[Random.Range(0, PS.Length)].GetComponent<DripSwitch>();
                time += 1;
            }
            else
                time = 0;

            drsw.isFlooding = true;
            drsw = null;
            //explosionSystem.Explode();
            waterTimer = 0.0f;
            BW.addWaterMultiplier(1.0f); //Remember to lower the mulitplier when the pipe gets fixed i.e. addWaterMultiplier(-1.0f);
        }

        if (Timer >= GameLength)
            endGameWin();
	}

    public void endGameLose()
    {
        SceneManager.LoadScene(0);
    }

    public void endGameWin()
    {
        //SceneManager.LoadScene(2);
        displayEndgame.SetActive(true);

        if (myScoreScript.player1Score + myScoreScript.player2Score > myScoreScript.player3Score + myScoreScript.player4Score)
        {
            endgame.text = "LEFT TEAM WINS";
        } else
        {
            endgame.text = "Right TEAM WINS";
        }
        
    }
}
