﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DripSwitch : MonoBehaviour {

    GameObject drip;
    GameObject flood;

    public bool isFlooding;
    public bool isFixed;

	// Use this for initialization
	void Start ()
    {
        isFlooding = false;
        isFixed = false;
        Transform[] children = GetComponentsInChildren<Transform>();

        for (int i = 0; i < children.Length; i++)
        {
            if (children[i].gameObject.tag == "WaterDrip")
            {
                drip = children[i].gameObject;
                drip.SetActive(false);
            }
        }

        for (int i = 0; i < children.Length; i++)
        {
            if (children[i].gameObject.tag == "WaterFlood")
            {
                flood = children[i].gameObject;
                flood.SetActive(false);
            }

        }
    }
	
	// Update is called once per frame
	void Update ()
    {
		if(isFlooding)
        {
            if(isFixed)
            {
                if (!drip.activeSelf)
                {
                    flood.SetActive(false);
                    drip.SetActive(true);
                }
            }

            else if(!isFixed)
            {
                if (!flood.activeSelf)
                {
                    flood.SetActive(true);
                    drip.SetActive(false);
                }
            }
        }
	}
}
