﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseWater : MonoBehaviour {

    public float WaterLevel;
    public float maxWaterLevel;
    public float WaterPercent;
    public float WaterMultiplier;

    public GameObject gameManager;

	// Use this for initialization
	void Start ()
    {
        WaterLevel = transform.position.y;
        WaterMultiplier = 1.0f;
    }
	
	// Update is called once per frame
	void Update ()
    {
        WaterLevel += Time.deltaTime * 0.01f * WaterMultiplier;

        WaterPercent = (WaterLevel / maxWaterLevel) * 100;

        if (WaterPercent >= 100.0f)
        {
            WaterLevel = maxWaterLevel;
			gameManager.GetComponent<ManagerOfGame>().endGameWin();
        }

        transform.position = new Vector3(0, WaterLevel, 0);
	}

    void SetWater(float Water)
    {
        WaterLevel = Water;
    }

    float GetWater()
    {
        return WaterLevel;
    }

    public void addWaterMultiplier(float multi)
    {
        WaterMultiplier += multi;
    }
}
