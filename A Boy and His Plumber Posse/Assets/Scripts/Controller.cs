﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using XboxCtrlrInput;
using XInputDotNetPure;

public class Controller : MonoBehaviour
{
    public XboxController controller;
    public Camera cam;

    [Space]

    public float walkSpeed;

    public float minimumX;
    public float maximumX;
    public float minimumY;
    public float maximumY;

    public float lookSensitivityX;
    public float lookSensitivityY;

    private Rigidbody rb;
    private Vector3 moveDirection;

    private float rotationX = 0.0f;
    private float rotationY = 0.0f;

    public GameObject triggerButton;

	// Use this for initialization
	void Awake ()
    {
        rb = GetComponent<Rigidbody>();
	}
	
	// Non-Physics steps
	void Update ()
    {
        rotationY += XCI.GetAxis(XboxAxis.RightStickX, controller) * lookSensitivityX;
        rotationX += XCI.GetAxis(XboxAxis.RightStickY, controller) * lookSensitivityY;

        rotationX = Mathf.Clamp(rotationX, minimumX, maximumX);

        //Get directional input from the user
        float horizontalMovement = XCI.GetAxis(XboxAxis.LeftStickX, controller);
        float verticalMovement = XCI.GetAxis(XboxAxis.LeftStickY, controller);

        moveDirection = (horizontalMovement * transform.right + verticalMovement * transform.forward).normalized;

        transform.localEulerAngles = new Vector3(0, rotationY, 0);
        cam.transform.localEulerAngles = new Vector3(-rotationX, 0, 0);
    }

    //Physics steps
    private void FixedUpdate()
    {
        Move();
    }

    void Move()
    {
        Vector3 yVelFix = new Vector3(0, rb.velocity.y, 0);
        rb.velocity = moveDirection * walkSpeed * Time.deltaTime;
        rb.velocity += yVelFix;
    }

    void DisplayTriggerButton()
    {
        //if within toolbox range
        triggerButton.SetActive(true);
        //else
        triggerButton.SetActive(false);
    }
}
