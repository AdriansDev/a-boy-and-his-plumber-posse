﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WallExplosion : MonoBehaviour
{

    public float Timer;
    GameObject wall;
    bool exploded;

    static Random rng = new Random();

	// Use this for initialization
	void Start ()
    {
        wall = gameObject;
        exploded = false;
	}
	
	// Update is called once per frame
	void Update ()
    {
    }

    public void Explode()
    {
        float forceX = Random.Range(-800, 800);
        float forceY = Random.Range(800, 1500);
        float forceZ = Random.Range(800, 1500);

        Rigidbody rb = wall.AddComponent<Rigidbody>();
        rb.mass = 1.0f;
        rb.collisionDetectionMode = CollisionDetectionMode.Continuous;
        rb.AddForce((transform.right * forceX) + (transform.up * (forceY / 2)) + (transform.forward * (forceZ * 2)));

        exploded = true;
    }
}
