﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using XboxCtrlrInput;

public class PlayerPickup : MonoBehaviour {

    public bool handsFull = false;
    public bool canPickUp = false;

    public bool plungerOn = false;
    
    public GameObject contextObject = null;
    Rigidbody rb = null;
    public SphereCollider trigger;
    public Vector3 PickUpPosition;


    public void SetPlungerOn(bool Plunger)
    {
        plungerOn = Plunger;
    }

    public bool GetPlunger()
    {
        return plungerOn;
    }
    // Use this for initialization
    void Start ()
    {
        trigger = GetComponentInChildren<SphereCollider>();
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (XCI.GetButtonDown(XboxButton.A) && canPickUp == true && handsFull == false && plungerOn == true)
        {
            if(contextObject != null)
            {
                contextObject.transform.parent = transform;
                contextObject.transform.localPosition = PickUpPosition;
                contextObject.transform.localRotation = Quaternion.Euler(0, 90.0f, 0f);
                rb = contextObject.GetComponent<Rigidbody>();
                rb.isKinematic = true;
                rb.useGravity = false;
                trigger.enabled = false;
                handsFull = true;
            }
        }

        else if (XCI.GetButtonDown(XboxButton.A) && contextObject != null && plungerOn == true)
        {
            if (contextObject != null)
            {
                contextObject.transform.parent = null;
                contextObject = null;
                rb.isKinematic = false;
                rb.useGravity = true;
                trigger.enabled = true;
                handsFull = false;
            }
        }
        
    }
    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == ("Able") || other.gameObject.tag == ("ToolBox"))
        {
            canPickUp = true;
            contextObject = other.gameObject;

        }
    }

    private void OnTriggerExit(Collider other)
    {
        if(other.gameObject.tag == ("Able") || other.gameObject.tag == ("ToolBox"))
        {
            canPickUp = false;
        }
    }
}
