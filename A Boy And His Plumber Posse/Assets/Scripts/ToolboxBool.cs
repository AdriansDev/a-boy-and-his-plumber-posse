﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ToolboxBool : MonoBehaviour {

    public GameObject player1;
    public GameObject player2;
    public GameObject player3;
    public GameObject player4;


    // Use this for initialization
    void Start ()
    {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject == player1)
        {
            player1.GetComponent<ToolSelect>().toolbox = true;
        }
        if (other.gameObject == player2)
        {
            player2.GetComponent<ToolSelect>().toolbox = true;
        }
        if (other.gameObject == player3)
        {
            player3.GetComponent<ToolSelect>().toolbox = true;
        }
        if (other.gameObject == player4)
        {
            player4.GetComponent<ToolSelect>().toolbox = true;
        }

    }
    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject == player1)
        {
            player1.GetComponent<ToolSelect>().toolbox = false;
        }
        if (other.gameObject == player2)
        {
            player2.GetComponent<ToolSelect>().toolbox = false;
        }
        if (other.gameObject == player3)
        {
            player3.GetComponent<ToolSelect>().toolbox = false;
        }
        if (other.gameObject == player4)
        {
            player4.GetComponent<ToolSelect>().toolbox = false;
        }
    }

}
